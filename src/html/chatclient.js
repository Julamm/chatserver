var ChatClient = {
	webSocket : null,
	chatboxView : null,
	chatInput : null,

	init : function(address) {
		chatboxView = document.getElementById("chatbox");
		chatInput = document.getElementById("chatInput");
		ChatClient.connect(address)
	},

	connect : function(address) {
		try {
			webSocket = new WebSocket(address);

			webSocket.onopen = function () {
				ChatClient.printText("Go ahead! Start chatting! :)");
			};

			webSocket.onmessage = function (msg) {
				ChatClient.printText(msg.data);
			};

			webSocket.onclose = function () {
				ChatClient.printText("Can't chat, no connection :(");
			};
		}
		catch (e) {
			console.error(e, e.stack);
		}
	},

	enterToSend : function(event) {
		if (event.key == "Enter") {
			ChatClient.sendText()
			chatInput.value = ''
		}
	},

	sendText : function() {
		var sendText = chatInput.value.trim();

		if (sendText == "") {
			return;
		}
		else {
			try {
				webSocket.send(sendText);
			}
			catch (e) {
				console.error(e, e.stack);
			}
		}
	},

	printText : function(text) {
		messageElement = document.createElement("p");
		messageElement.innerText = text;
		chatboxView.appendChild(messageElement);
		chatboxView.scrollTop = chatboxView.scrollHeight;
	},

	selectAll : function() {
		chatInput.select();
	}
};
