#ifndef SERVER_H
#define SERVER_H

#include <boost/asio/ip/tcp.hpp>
#include <memory>
#include <string>
#include "logger.hpp"
#include "room.h"
#include "settings.hpp"

namespace asio = boost::asio;
using tcp = boost::asio::ip::tcp;

class Server
{
public:
	Server(const Settings& settings, Logger& logger);
	Server(const Server&) = delete;
	Server& operator=(const Server&) = delete;
	void run();
	void exit();

private:
	asio::io_context m_ioc{1};
	std::unique_ptr<tcp::acceptor> m_acceptor;
	const Settings& m_settings;
	uint64_t m_idCounter;
	std::shared_ptr<Room> m_room;
	Logger& m_logger;

	void accept();
	void acceptHandler(const boost::system::error_code& error,
					   asio::ip::tcp::socket peer);
};

#endif
