#ifndef LOGGER_HPP
#define LOGGER_HPP

#include "time.hpp"
#include <cstring>
#include <iostream>
#include <iterator>
#include <mutex>
#include <chrono>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <filesystem>
#include <deque>

#ifdef _MSC_VER
#define __FILENAME__ (strrchr("\\" __FILE__, '\\') + 1)
#else
#define __FILENAME__ (strrchr("/" __FILE__, '/') + 1)
#endif

#define FILELOCATION (std::string(__FILENAME__) + ":" + std::to_string(__LINE__))
#define STRINGIFY(ARG) static_cast<std::ostringstream &&>((std::ostringstream() << ARG)).str()

#define LOG(LOGGER, ARG) LOGGER.log(STRINGIFY(ARG), FILELOCATION)
#define WARNING(LOGGER, ARG) LOGGER.warning(STRINGIFY(ARG), FILELOCATION)
#define ERROR(LOGGER, ARG) LOGGER.error(STRINGIFY(ARG), FILELOCATION)
#ifdef DEBUG_MODE
#define DEBUG(LOGGER, ARG) LOGGER.debug(STRINGIFY(ARG), FILELOCATION)
#else
#define DEBUG(ARG) do{}while(0)
#endif

#define SIZE_PER_FILE (20 * 1000000)

class Logger
{
public:
	Logger(bool logToFile = false)
		: m_logToFile(logToFile)
	{
		if (m_logToFile)
		{
			m_pastFiles.emplace_back("chatserver-" + getTimestamp() + ".log");
		}
	}

	inline void log(const std::string& data, const std::string& fileAndLine)
	{
		std::string output = "[" + getTimestamp() + "] (" + fileAndLine + ") LOG: " + data;
		out(output, std::cout);
	}

	inline void warning(const std::string& data, const std::string& fileAndLine)
	{
		std::string output;
		if (m_logToFile)
		{
			output = "[" + getTimestamp() + "] (" + fileAndLine + ") WARNING: " + data;
		}
		else
		{
			output = "\033[0;33m[" + getTimestamp() + "] (" + fileAndLine + ") WARNING:\033[0m " + data;
		}
		out(output, std::cerr);
	}

	inline void error(const std::string& data, const std::string& fileAndLine)
	{
		std::string output;
		if (m_logToFile)
		{
			output = "[" + getTimestamp() + "] (" + fileAndLine + ") ERROR: " + data;
		}
		else
		{
			output = "\033[0;31m[" + getTimestamp() + "] (" + fileAndLine + ") ERROR:\033[0m " + data;
		}
		out(output, std::cerr);
	}

	inline void debug(const std::string& data, const std::string& fileAndLine)
	{
		std::string output = "[" + getTimestamp() + "] (" + fileAndLine + ") DEBUG: " + data;
		out(output, std::cout);
	}

	inline void out(const std::string& data, std::ostream& os)
	{
		std::lock_guard<std::mutex> guard(mx);
		if (!m_logToFile)
		{
			os << data << std::endl;
		}
		else
		{
			std::ofstream stream(m_pastFiles.back(), std::ios_base::app);
			stream << data << std::endl;
			std::uintmax_t size = std::filesystem::file_size(m_pastFiles.back());
			if (size > SIZE_PER_FILE)
			{
				m_pastFiles.emplace_back("chatserver-" + getTimestamp() + ".log");
				if (m_pastFiles.size() > 2)
				{
					std::filesystem::remove(m_pastFiles.front());
					m_pastFiles.pop_front();
				}
			}
		}
	}

private:
	std::mutex mx;
	std::deque<std::string> m_pastFiles;
	bool m_logToFile;
};

#endif
