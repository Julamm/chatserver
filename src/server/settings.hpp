#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <string>

struct Settings
{
	std::string address = "0.0.0.0";
	unsigned short port = 8080;
	bool log = false;
	unsigned int messageMaxSize = 200;
	unsigned int rateLimitMs = 10;
	uint64_t startId = 1;
};

#endif
