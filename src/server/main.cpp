#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <tuple>
#include <unistd.h>
#include "client.h"
#include "room.h"
#include "server.h"
#include "settings.hpp"
#include "settingsReader.h"
#include "logger.hpp"

std::unique_ptr<Server> server;

void exitHandler(int s)
{
	std::ignore = s;
	server->exit();
}

int main(int argc, char* argv[])
{
	struct sigaction sigIntHandler;
	sigIntHandler.sa_handler = exitHandler;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;
	sigaction(SIGINT, &sigIntHandler, NULL);

	Settings settings;
	bool configfileIsPresent = std::filesystem::exists("defaults.conf");
	if (configfileIsPresent)
	{
		SettingsReader::parse("defaults.conf", settings);
	}

	Logger logger(settings.log);
	if (!configfileIsPresent)
	{
		WARNING(logger, "No config file read, using default values");
	}

	if (argc == 3)
	{
		settings.address = argv[1];
		settings.port = static_cast<unsigned short>(std::atoi(argv[2]));
	}

	server = std::make_unique<Server>(settings, logger);
	server->run();
}
