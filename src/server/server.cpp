#include "server.h"
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <functional>

Server::Server(const Settings& settings, Logger& logger)
	: m_settings(settings)
	, m_idCounter(m_settings.startId)
	, m_logger(logger)
{
	LOG(m_logger, "Server starting up...");
	auto const boostAddr = asio::ip::make_address(m_settings.address);
	m_acceptor = std::unique_ptr<tcp::acceptor>(new tcp::acceptor{m_ioc, {boostAddr, m_settings.port}});
	m_room = std::make_shared<Room>(m_logger);
	LOG(m_logger, "Server binded to: " << m_acceptor->local_endpoint().address().to_string() << " " << m_acceptor->local_endpoint().port());
}

void Server::run()
{
	accept();
	m_ioc.run();
	LOG(m_logger, "Server stopped running");
}

void Server::accept()
{
	m_acceptor->async_accept(std::bind(&Server::acceptHandler,
									   this, std::placeholders::_1, std::placeholders::_2));
	DEBUG(m_logger, "async_accept()");
}

void Server::acceptHandler(const boost::system::error_code& error,
						   asio::ip::tcp::socket peer)
{
	DEBUG(m_logger, "acceptHandler()");
	if (!error)
	{
		std::string ip = peer.remote_endpoint().address().to_string();
		uint64_t id = m_idCounter++;
		LOG(m_logger, "Client connected from " << ip << " with id \"" << id << "\"");

		std::shared_ptr<Client> client = std::make_shared<Client>(std::move(peer), id, m_logger);
		client->set(m_room);
		client->run();
		m_room->join(client);
	}

	accept();
}


void Server::exit()
{
	m_ioc.stop();
}
