#ifndef ROOM_H
#define ROOM_H

#include <vector>
#include <memory>
#include <string>
#include <mutex>
#include <deque>
#include <utility>
#include "client.h"
#include "logger.hpp"

class Room
{
public:
	Room(Logger& logger);
	void share(const std::string& message);
	void join(std::shared_ptr<Client> client);
	void leave(std::shared_ptr<Client> client);

private:
	std::vector<std::shared_ptr<Client>> m_clients;
	std::mutex m_utex;
	std::deque<std::string> m_messageHistory;
	Logger& m_logger;

	void sendAllClients(const std::string& message);
};

#endif
