CXX:=g++
CXXFLAGS:=-Wall -Wextra -pedantic -std=c++17
RELEASEF:=-O3
DEBUGF:=-ggdb -O0 -DDEBUG_MODE
LDFLAGS:=-lboost_system -lpthread -lstdc++fs

CPPFLAGS:=-I./src/server

SRC:=$(wildcard src/server/*.cpp)
OBJ:=$(patsubst %.cpp,%.o, $(SRC))

PRG:=chatbox-server

.PHONY:all clean

all:debug

release:CXXFLAGS += $(RELEASEF)
release:$(PRG)
	@echo "Release build"

debug:CXXFLAGS += $(DEBUGF)
debug:$(PRG)
	@echo "Debug build"

$(PRG):$(OBJ)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $^ -o $@ $(LDFLAGS)

%.o:%.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c $< -o $@ $(LDFLAGS)

clean:
	rm -rf $(OBJ)
	rm -rf $(PRG)
